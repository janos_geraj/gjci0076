package BibliotecaApp.model;

import org.junit.*;

import static org.junit.Assert.*;

public class CarteTest {
    Carte c1, c2, c3;

    @BeforeClass
    public static void setUpClass() {
        System.out.println("Initializing test");
        System.out.println("----------------");
    }
    @Before
    public void setUp() throws Exception {
        c1 = Carte.getCarteFromString("Amintiri din copilarie;Ion Creanga;1888;Editas;ion,creanga,amintiri,din,copilarie");
        c2 = Carte.getCarteFromString("Luceafarul;Mihai Eminescu;1834;Editas;luceafarul,mihai,eminescu,editas");
        c3 = Carte.getCarteFromString("Plumb;George Bacovia;1857;Editas;plumb,george,bacovia,editas");
    }

    @After
    public void tearDown() throws Exception {
        c1 = c2 = c3 = null;
    }
    @AfterClass
    public static void tearDownClass() {
        System.out.println("Test complete");
    }

    @Test
    public void getTitlu() {
        System.out.println("Test: getTitlu");
        assertEquals("Amintiri din copilarie", c1.getTitlu());
        assertEquals("Luceafarul", c2.getTitlu());
        assertEquals("Plumb", c3.getTitlu());
        System.out.println("Test passed");
        System.out.println("----------------");
    }

    @Test
    public void getAnAparitie() {
        System.out.println("Test: getAnAparitie");
        assertEquals("1834", c2.getAnAparitie());
        assertEquals("1888", c1.getAnAparitie());
        assertEquals("1857", c3.getAnAparitie());
        System.out.println("Test passed");
        System.out.println("----------------");
    }

    @Test
    public void getEditura() {
        System.out.println("Test: getEditura");
        assertEquals("Editas", c3.getEditura());
        assertEquals("Editas", c2.getEditura());
        assertEquals("Editas", c1.getEditura());
        System.out.println("Test passed");
        System.out.println("----------------");
    }


}