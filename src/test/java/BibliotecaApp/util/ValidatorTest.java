package BibliotecaApp.util;

import BibliotecaApp.model.Carte;
import org.junit.*;

import static org.junit.Assert.*;

public class ValidatorTest {
    Validator v = new Validator();
    Carte c1, c2, c3, c4, c5;
    @BeforeClass
    public static void setUpClass() throws Exception {
        System.out.println("Begin Test");
        System.out.println("------------");
    }

    @Before
    public void setUp() throws Exception {
        c1 = Carte.getCarteFromString("Titlu;Autor;1990;Editas;cuv,cheie");  //CAZ CORECT
        c2 = Carte.getCarteFromString("Titlu;;1990;Editas;cuv,cheie");       //fara autor
        c3 = Carte.getCarteFromString("Titlu;Autor;anii;Editas;cuv,cheie");  //anul nu e numar
        c4 = Carte.getCarteFromString("$%&*#;Autor;1990;Editas;cuv,cheie");  //titlu invalid
        c5 = Carte.getCarteFromString("Titlu;((#@^!;1990;Editas;cuv,cheie"); //autor invalid
    }
    @Test
    public void isStringOKTest() throws Exception {
        assertTrue("it passes1", v.isStringOK("ygaefuybenwougvbecwi"));
        assertTrue("it passes2", v.isStringOK("OOSDBEBYVBCEDNSJSUDU"));
        assertTrue("it passes3", v.isStringOK("ygaeOOSDBtsdvSDJYVwi"));
        assertTrue("it passes4", v.isStringOK("SGDYysgdYSjskivbecwi"));
        assertTrue("it passes5", v.isStringOK("ygaefuybenwGDhsDHSUJ"));
        assertTrue("it passes6", v.isStringOK("ygaSAsauhHUASkSAecwi"));
        //assertTrue("it CRASHES", v.isStringOK("1324"));
        //assertNotSame("works!", v.isStringOK("1324"));
        //assertFalse("it CRASHES", v.isStringOK("-"));
        //assertTrue("it CRASHES", v.isStringOK("$#%@^&@*#&^@$"));
        System.out.println("PASSED isStringOKTest !");
    }

    @Test
    public void isStringOKTestInvalidString() {
        boolean thrown = false;

        try {
            Validator.isStringOK("----");
        } catch (Exception e) {
            thrown = true;
        }

        assertTrue(thrown);
        System.out.println("PASSED isStringOKTestInvalidString");
    }


    @Test
    public void validateCarteTest() throws Exception {

    }


    @After
    public void tearDown() throws Exception {

    }

    @AfterClass
    public static void tearDownClass() throws Exception {
        System.out.println("------------");
        System.out.println("Tear Down Test");
    }
}