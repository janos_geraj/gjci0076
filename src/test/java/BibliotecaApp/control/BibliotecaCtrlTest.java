package BibliotecaApp.control;

import BibliotecaApp.model.Carte;
import BibliotecaApp.model.repo.CartiRepo;
import org.junit.*;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.fail;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class BibliotecaCtrlTest {

    /*
    @BeforeClass
    public static void setUp() throws Exception {
        System.out.println("S-a inceput testarea");
    }

    Carte c1 = new Carte();
    Carte c2 = new Carte();
    CartiRepo repo = new CartiRepo();
    BibliotecaCtrl ctrl = new BibliotecaCtrl(repo);
    List<Carte> car = new ArrayList<Carte>();

    @Before
    public void setup() throws Exception {
        System.out.println("Creez cartea cu autor 'unAutor'");
        c1 = Carte.getCarteFromString("Amintiri din copilarie;unAutor;1888;Editas;Ion,Creanga");
        ctrl.adaugaCarte(c1);
        System.out.println("-------------------------------------------");
        System.out.println("Am adaugat cartea c1");
        System.out.println(c1);
    }

    @Test
    public void test1() throws Exception {
        System.out.println(ctrl.cautaCarte("unAutor"));
        Assert.assertEquals("[Amintiri din copilarie;unAutor;1888;Editas;Ion,Creanga]",
                ctrl.cautaCarte("unAutor").toString());
    }

    @After
    public void teardown() {
        System.out.println("teardown");
        ctrl = null;
    }

    @AfterClass
    public static void tearDown() {
        System.out.println("S-a terminat testarea");
    }
    */

    Carte c1 = new Carte();
    Carte c2 = new Carte();
    Carte c3 = new Carte();
    CartiRepo repo = new CartiRepo();
    BibliotecaCtrl ctrl = new BibliotecaCtrl(repo);
    List<Carte> car = new ArrayList<Carte>();

    @Before
    public void setup() throws Exception {
        System.out.println("Creez cartea Toamna;Ion Ionel;1900;Humanitas;toamna");
        c1 = Carte.getCarteFromString("Toamna;Ion Ionel;1900;Humanitas;toamna");
        System.out.println("Am adaugat cartea c1");
        System.out.println("-------------------------------------------");
    }

    @Test
    public void test1() throws Exception {
        System.out.println(ctrl.cautaCarte("unAutor"));
        Assert.assertEquals("[Toamna;Ion Ionel;1900;Humanitas;toamna]",
                ctrl.cautaCarte("Ion Ionel").toString());
        System.out.println("Am adaugat cartea");
    }

    @Test
    public void test2() throws Exception {
            try {
                System.out.println("Incerc sa creez cartea abc;Liviu Livada;sha256;Edtas;aaa");
                c2 = Carte.getCarteFromString("abc;Liviu Livada;sha256;Edtas;aaa");
                ctrl.adaugaCarte(c2);
                System.out.println("-------------------------------------------");
                System.out.println(c2);
                fail("Expected editura invalida exception");
            } catch (Exception e) {
                assertThat(e.getMessage(), is("Editura invalid!"));
            }
        System.out.println("Anul introdus este gresit. Introdu numai numere!");
        }

    @Test
    public void test3() throws Exception {
        try {
            System.out.println("Incerc sa creez cartea ;Gheorghe Ger;1997;Humanitas;bcd");
            c3 = Carte.getCarteFromString(";Gheorghe Ger;1997;Humanitas;bcd");
            ctrl.adaugaCarte(c3);
            System.out.println("-------------------------------------------");
            System.out.println(c3);
            fail("Expected titlu invalid exception");
        } catch (Exception e) {
            assertThat(e.getMessage(), is("String invalid"));
        }
        System.out.println("Titlul este gresit!");
    }

    @After
    public void teardown() {
        System.out.println("teardown");
        ctrl = null;
    }

    @AfterClass
    public static void tearDown() {
        System.out.println("Test finalizat cu succes");



    }

}
